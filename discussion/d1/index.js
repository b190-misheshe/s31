/*
require - use load node.js module
module - software component / part of a program that contains one or more routines
http - lets node.js transfer data using hyper text transfer protocol
*/
let http = require("http");

http.createServer(function (request,response){
  response.writeHead(200,{"Content-Type": "text-plain"});
  response.end("Hello World");
}).listen(4000);

console.log("Server running at port:4000");