const http = require ("http");

// Storing the 400 in  a variable called port
const port = 4000;

// Storing the createServer method inside the server variable
// const server = http.createServer(function(require, response){
// });
// server.listen(port);

// console.log(`Server now running a port: ${port}`);

// output: 
// [nodemon] 2.0.19
// [nodemon] to restart at any time, enter `rs`
// [nodemon] watching path(s): *.*
// [nodemon] watching extensions: js,mjs,json
// [nodemon] starting `node routes index.js`
// Server running at port:4000

const server = http.createServer((request, response) => {
  if (request.url === "/greeting") {
  response.writeHead(200,{"Content-Type": "text-plain"});
  response.end("Hello Again");
  } 
  else if (request.url === "/homepage") {
  response.writeHead(200,{"Content-Type": "text-plain"});
  response.end("Hello");
  } 
  else {
  response.writeHead(200,{"Content-Type": "text-plain"});
  response.end("Page not found");
  } 
});

server.listen(port);

console.log(`Server now running a port: ${port}`);

